/** \file main.c
 * \brief Classe principale du programme.
 * \version 1.0
 * \author Yann et Wassim
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include "codeur.h"

void viderBuffer();
int lire(char *chaine, int longueur);

int main()
{
    int option;
    char fichierSrc[50], fichierArbre[50];
    printf("1: coder un fichier\n2: decoder un fichier\nChoix :");
    scanf("%d", &option);

    while(option < 1 || option > 2) {
        viderBuffer();
        printf("ERREUR ! Veuillez choisir une valeur entre celles ci : \n1: coder un fichier\n2: decoder un fichier\nChoix :");
        scanf("%d", &option);
    }

    if(option == 1) /**< Si cette condition est vraie, on code un fichier */
    {
        /* **** Chemin fichier � coder **** */
        printf("Chemin du fichier a coder : ");
        viderBuffer();
        lire(fichierSrc, 50);

        while(strcmp(fichierSrc,"N") != 0 && test(fichierSrc) == 0) { /**< Verifier que le chemin lu est valide... */
            printf("\nLe fichier est introuvable ou ne peut �tre lu! Reessayez ou entrez N pour quitter :");
            lire(fichierSrc, 50);
        }
        if(strcmp(fichierSrc, "N") == 0) {
            exit(0);
        }

        lireFichier(fichierSrc); /**< Code le fichier... */
    } else if (option == 2) /**< Sinon on d�code un fichier */
    {
        /* **** Chemin fichier � d�coder **** */
        printf("Chemin du fichier a decoder : ");
        viderBuffer();
        lire(fichierSrc, 50);

        while(strcmp(fichierSrc,"N") != 0 && test(fichierSrc) == 0) { /**< Verifier que le chemin lu est valide... */
            printf("\nLe fichier est introuvable ou ne peut �tre lu! Reessayez ou entrez N pour quitter :");
            lire(fichierSrc, 50);
        }
        if(strcmp(fichierSrc, "N") == 0) {
            exit(0);
        }

        /* **** Chemin de l'arbre � d�coder **** */
        printf("Chemin de l'arbre du fichier : ");
        lire(fichierArbre, 50);

        while(strcmp(fichierArbre,"N") != 0 && test(fichierArbre) == 0) { /**< Verifier que le chemin lu est valide... */
            printf("\nLe fichier est introuvable ou ne peut �tre lu! Reessayez ou entrez N pour quitter :");
            lire(fichierArbre, 50);
        }
        if(strcmp(fichierArbre, "N") == 0) {
            exit(0);
        }

        decoderFichier(fichierSrc, fichierArbre); /**< Code le fichier... */
    }
    return 0;
}

/** \brief Vide le buffer et �vite qu'il ne reste des caract�res d'anciennes entr�es.
 *
 * \return void
 *
 */
void viderBuffer()
{
    int c = 0;
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}

/** \brief Permet de lire les entr�es de mani�re securis�e.
 *
 * \param chaine char*
 * \param longueur int
 * \return int
 *
 */
int lire(char *chaine, int longueur)
{
    char *positionEntree = NULL;

    if (fgets(chaine, longueur, stdin) != NULL)
    {
        positionEntree = strchr(chaine, '\n');
        if (positionEntree != NULL)
        {
            *positionEntree = '\0';
        }
        else
        {
            viderBuffer();
        }
        return 1;
    }
    else
    {
        viderBuffer();
        return 0;
    }
}
