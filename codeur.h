/** \file codeur.h
 * \brief Ent�te de la classe codeur.
 * \version 1.0
 * \author Yann et Wassim
 */

#ifndef codeur_h
#define codeur_h


/** \struct node codeur.h
 * \brief D�finition de la structure d'une Node.
 */
typedef struct node node, *Arbre, *List;
struct node
{
    bool fusion;
    int nombre;
        struct{struct node *suivant; char caractere;};
        struct{struct node *gauche, *droite;};
};


/** \struct File codeur.h
 * \brief D�finition de la structure d'une liste.
 */
typedef struct
{
    List premier, dernier;
}File;


/** \struct elem codeur.h
 * \brief D�finition de la structure d'une �l�ment d'un dictionnaire.
 */
struct elem
{
    char cle[255];
    char valeur;
};
typedef struct elem element;

/** \struct dictionnaire codeur.h
 * \brief D�finition de la structure d'un dictionnaire
 */
struct dico
{
    element* elements;
    int taille;
};
typedef struct dico dictionnaire;


void ajoutElement(List *list, char c);
void trierList(List actuel);
void afficherList(List actuel);
int tailleList(List actuel);
int test(char *fname);
node* recupererPlusPetit(List *list, File *file);
void afficherArbre( Arbre arbre, int n);
void fusionnerDeuxPlusPetit( List *list, File *file);
void code( Arbre arbre, char c, int tab[], int n, FILE* fichier);
void donneCodage( Arbre arbre, char c, int tab[], int n, FILE* fichier);
int lireFichier(char *fnameL);
int decoderFichier(char*fnameL, char* fnameA);
dictionnaire* creation_dictionnaire(char* cle, char* valeur);
dictionnaire* ajout_tete(dictionnaire* mon_dico, char* cle, char* valeur);
void affichage(dictionnaire* mon_dico);
dictionnaire* tabCodage(FILE* fichier);

#endif // codeur_h
