/** \file codeur.c
 * \brief Classe contenant les méthodes du decodeur.
 * \version 1.0
 * \author Yann et Wassim
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "codeur.h"
#include <limits.h>


/** \brief Crée un dictionnaire de donnée qui servira à stocker les caractères codées et leurs valeurs.
 *
 * \param cle char*
 * \param valeur char*
 * \return dictionnaire*
 *
 */
dictionnaire* creation_dictionnaire(char *cle, char* valeur)
{
    dictionnaire* dico = (dictionnaire*) malloc(sizeof(dictionnaire));
    dico->elements = (element*) malloc(sizeof(element));
    strcpy(dico->elements->cle, cle);
    dico->elements->valeur = valeur;
    dico->taille = 1;
    return dico;
}


/** \brief Ajoute un element au dictionnaire de données.
 *
 * \param mon_dico dictionnaire*
 * \param cle char*
 * \param valeur char*
 * \return dictionnaire*
 *
 */
dictionnaire* ajout_tete(dictionnaire* mon_dico, char* cle, char* valeur)
{
    mon_dico->taille++;
    mon_dico->elements = realloc(mon_dico->elements, sizeof(element) * mon_dico->taille);
    strcpy(mon_dico->elements[mon_dico->taille-1].cle, cle);
    mon_dico->elements[mon_dico->taille-1].valeur = valeur;
    return mon_dico;
}


/** \brief Affiche les données du dictionnaire.
 *
 * \param mon_dico dictionnaire*
 * \return void
 *
 */
void affichage(dictionnaire* mon_dico)
{
    printf("{");
    printf("%s", mon_dico->elements[0].cle);
    printf(":%c", mon_dico->elements[0].valeur);
    for(int i = 1; i < mon_dico->taille; i++)
    {
        printf(", %s", mon_dico->elements[i].cle);
        printf(":%c", mon_dico->elements[i].valeur);
    }
    printf("}");
}

/* *** Fin dictionnaire *** */


/** \brief Ajout d'un élément dans l'Arbre.
 *
 * \param list List*
 * \param c char
 * \return void
 *
 */
void ajoutElement(List *list, char c)
{
    node *actuel = *list;

    while (actuel != NULL)
    {
        if( c == actuel->caractere)
        {
            actuel->nombre = actuel->nombre+1; /**< Incrémentation d'une Node déjà existante */
            return;
        }
        actuel = actuel->suivant;
    }

    node *nouveau = malloc(sizeof(*nouveau)); /**< Création d'une nouvelle Node */
    nouveau->caractere = c;
    nouveau->nombre=1;
    nouveau->fusion=0;

    nouveau->suivant = *list; /**< Insertion de l'élément au début de l'Arbre */
    *list = nouveau;
}


/** \brief  Trie une liste d'elements.
 *
 * \param actuel List
 * \return void
 *
 */
void trierList(List actuel)
{
    node *s;
	for( actuel; actuel != NULL; actuel = actuel->suivant)
	{
		for(s = actuel; s != NULL; s = s->suivant)
		{
			if(actuel->nombre > s->nombre)
			{
				int c = actuel->caractere;
				int nb = actuel->nombre;
				actuel->caractere = s->caractere;
				actuel->nombre = s->nombre;
				s->caractere = c;
				s->nombre = nb;
			}
		}
	}
}


/** \brief Affiche les valeurs d'une liste de caractère et leur occurence.
 *
 * \param actuel List
 * \return void
 *
 */
void afficherList(List actuel)
{
    while (actuel != NULL)
    {
        printf("%c : ", actuel->caractere);
        printf("%d -> ", actuel->nombre);
        actuel = actuel->suivant;
    }
    printf("End\n");
}


/** \brief Écriture de l'arbre.
 *
 * \param arbre Arbre
 * \param c char
 * \param tab[] int
 * \param n int
 * \param actuel List
 * \return void
 *
 */
void afficherCodeCaractere( Arbre arbre, char c, int tab[], int n, List actuel)
{
    char *fnameE = "Arbre.txt";
    FILE* fichierE = fopen( fnameE, "w");
    while (actuel != NULL)
    {
        donneCodage( arbre, actuel->caractere, tab, n, fichierE);
        actuel = actuel->suivant;
    }
    fclose(fichierE);
}


/** \brief Retourne la taille d'une liste.
 *
 * \param actuel List
 * \return int
 *
 */
int tailleList(List actuel)
{
    int compteur = 0;
    while (actuel != NULL)
    {
        compteur++;
        actuel = actuel->suivant;
    }
    printf("La liste possède %d caractères.\n", compteur);
    return compteur;
}



/** \brief Test l'ouverture du fichier associé au chemin passé en paramètre.
 *
 * \param fname char*
 * \return int
 *
 */
int test(char* fname)
{
    FILE *fichier = fopen(fname, "r+");

    if (fichier != NULL)
    {
        return 1;
    }

    return 0;
}


/** \brief Recupère le plus petit élément d'une liste.
 *
 * \param list List*
 * \param file File*
 * \return node*
 *
 */
node* recupererPlusPetit(List *list, File *file)
{
    List actuel= *list, premier=file->premier;
    if( actuel == NULL && premier == NULL)
        return NULL;
    if( premier == NULL || (actuel != NULL && actuel->nombre < premier->nombre))
    {
        *list = actuel->suivant;
        return actuel;
    }
    file->premier = premier->suivant;
    return premier;
}


/** \brief Affiche l'arbre crée.
 *
 * \param arbre Arbre
 * \param n int
 * \return void
 *
 */
void afficherArbre( Arbre arbre, int n)
{
    if( arbre->fusion)
    {
        printf("%*d\n", 3*n, arbre->nombre);
        afficherArbre( arbre->gauche, n+1);
        afficherArbre( arbre->droite, n+1);
    }
    else
    {
        printf("%*d %c\n", 3*n, arbre->nombre, arbre->caractere);
    }
}


/** \brief Fusionne deux branches en une seule.
 *
 * \param list List*
 * \param file File*
 * \return void
 *
 */
void fusionnerDeuxPlusPetit( List *list, File *file)
{
    List l1=recupererPlusPetit( list, file);
    List l2=recupererPlusPetit( list, file);
    if( l1 == NULL)
        return;
    if( l2 != NULL)
    {
        node *nouveau = malloc(sizeof(*nouveau));
        nouveau->fusion=1;
        nouveau->nombre=l1->nombre+l2->nombre;
        nouveau->droite=l1;
        nouveau->gauche=l2;
        l1 = nouveau;
    }
    if( file->premier == NULL)
        file->premier=l1;
    else
        file->dernier->suivant=l1;
    file->dernier=l1;
    l1->suivant=NULL;
}


/** \brief Code les caractères d'un arbre.
 *
 * \param arbre Arbre
 * \param c char
 * \param tab[] int
 * \param n int
 * \param fichier FILE*
 * \return void
 *
 */
void code( Arbre arbre, char c, int tab[], int n, FILE* fichier)
{
    if( arbre->fusion)
    {
        tab[n]=0;
        code( arbre->gauche, c, tab, n+1, fichier);
        tab[n]=1;
        code( arbre->droite, c, tab, n+1, fichier);
    }
    else
    {
        if( arbre->caractere==c)
        {
            for( int i = 0; i<n; i++)
            {
                fprintf( fichier, "%d", tab[i]);
            }
        }
    }
}

/** \brief Affiche le codage des caractères.
 *
 * \param arbre Arbre
 * \param c char
 * \param tab[] int
 * \param n int
 * \param fichier FILE*
 * \return void
 *
 */
void donneCodage( Arbre arbre, char c, int tab[], int n, FILE* fichier)
{
    if( arbre->fusion)
    {
        tab[n]=0;
        donneCodage( arbre->gauche, c, tab, n+1, fichier);
        tab[n]=1;
        donneCodage( arbre->droite, c, tab, n+1, fichier);
    }
    else
    {
        if( arbre->caractere==c)
        {
            printf("'%c' -> ", c);
            fprintf( fichier, "%c:", c);
            for( int i = 0; i<n; i++)
            {
                printf("%d", tab[i]);
                fprintf( fichier, "%d", tab[i]);
            }
            printf("\n");
            fprintf( fichier, "\n");
        }
    }
}


/** \brief Retourne un dictionnaire contenant les lettres et codes associés recuperés dans un fichier d'arbre.
 *
 * \param fichier FILE*
 * \return dictionnaire*
 *
 */
dictionnaire* tabCodage(FILE* fichier){
    int i;
    int currentL = 0;
    dictionnaire* tableDecodage;
    char* lettreActuelle;
    char* code = (int*)malloc(sizeof(int)*1);
    while(1)
    {
        currentL = fgetc(fichier);
        if(currentL == EOF)
            break;
        lettreActuelle = (char) currentL;
        currentL = fgetc(fichier);
        i = 0;
        while(currentL != '\n' && currentL != EOF) {
            currentL = fgetc(fichier);
            code[i] = (char) currentL;
            if(code[i] == ':') continue;
            i++;
            code = (char *)realloc(code, sizeof(char)*i*2);
        }
        code[i-1] = '\0';
        if(tableDecodage == NULL) {
            tableDecodage = creation_dictionnaire(code, lettreActuelle);
        } else {
            tableDecodage = ajout_tete(tableDecodage, code, lettreActuelle);
        }
        code = (char *)realloc(code, sizeof(char)*1);
    }
    return tableDecodage;
}

/** \brief Permet de générer un fichier décompressé à partir d'un fichier compressé et de son arbre.
 *
 * \param fnameL char*
 * \param fnameA char*
 * \return int
 *
 */
int decoderFichier(char* fnameL, char* fnameA)
{
    FILE* fichierL = fopen( fnameL, "rb"); //Ouvre le fichier
    FILE* fichierA = fopen(fnameA, "r"); // ouvre l'arbre

    int i, j;
    char* codageTab = (char*)malloc(sizeof(char)*1);
    dictionnaire* tabDecodage = tabCodage(fichierA);
    affichage(tabDecodage);

    /* extraction du fichier codé... */
    unsigned long fileLen;
    fseek(fichierL, 0, SEEK_END);
    fileLen=ftell(fichierL);
    fseek(fichierL, 0, SEEK_SET);
    char* buffer = (char*)malloc((fileLen+1)*sizeof(char));
    fread(buffer, fileLen, 1, fichierL);

    printf("\nFichier en binaire...\n");
    for(int fk = 0;fk < fileLen;++fk)
        printf("%c", buffer[fk]);

    printf("\n\nOK! Fichier en cours de decodage...\n");
    i = 0;
    int estTrouve = 0;
    char* tester = (char*)malloc(sizeof(char)*1);

    char *fnameDec = "fichierDecompresse.txt";
    FILE* fichierDec = fopen(fnameDec, "w");
    for(int f = 0; f < fileLen; f++)
    {
        tester[i] = (char) buffer[f];
        // init tester at 1 length
        for(int b = (i+1); b < strlen(tester); b++){
            tester[b] = NULL;
        }
        for(int j = 0; j < tabDecodage->taille; j++)
        {
            //printf("\n%.*s COMPARED TO %s = %d TAILLE code : %d TAILLE cle : %d", i+1, tester, tabDecodage->elements[j].cle, strcmp(tabDecodage->elements[j].cle, tester), strlen(tester), strlen(tabDecodage->elements[j].cle));
            if(strcmp(tabDecodage->elements[j].cle, tester) == 0){
            //if(strncmp(tabDecodage->elements[j].cle, tester, strlen(tabDecodage->elements[j].cle)) == 0){

                /* *** AJOUTE LE CARACTERE AU FICHIER *** */
                //printf("%c", tabDecodage->elements[j].valeur);
                fprintf(fichierDec, "%c", tabDecodage->elements[j].valeur);
                /* *** FIN AJOUT *** */
                // remettre la chaine de caractère à 0
                tester = (char *)realloc(tester, sizeof(char)*1);
                for(int b = 1; b < strlen(tester); b++){
                    tester[b] = NULL;
                }
                i = 0;
                tester[i] = NULL;
                estTrouve = 1;
                break;
            }
        }
        if(estTrouve == 0){
            //printf("\n%.*s", i+1, tester);
            i++;
            tester = (char *)realloc(tester, sizeof(char)*(i+1));
        } else {
            estTrouve = 0;
        }
    }

    printf("\n\nOK! FICHIER DECODE!");
    fclose(fichierL);
    fclose(fichierA);
    return 1;

}


/** \brief Crée un arbre trié à partir de caractères tirés d'un fichier texte.
 *
 * \param fnameL char*
 * \return int
 *
 */
int lireFichier(char *fnameL)
{
    FILE* fichierL = fopen( fnameL, "r"); //Ouvre le fichier

    if (fichierL != NULL)
    {
        List list = NULL;
        List list2 = NULL;
        File file = {NULL, NULL};
        int caractereActuel = 0;
        while(1)
        {
            caractereActuel = fgetc(fichierL); /**< Récuppère le caractère */
            if( caractereActuel == EOF)
                break;
            ajoutElement( &list, caractereActuel); /**< Crée une Node avec le caractère */
        }

        fclose(fichierL);
        trierList( list);
        while( list != NULL || file.premier!= NULL && file.premier!=file.dernier)
        {
            fusionnerDeuxPlusPetit( &list, &file);
        }
        afficherArbre( file.premier, 0);
        printf("\n\n");
        char *fnameE = "fichierCompresse.txt";
        FILE* fichierE = fopen(fnameE, "w");
        fichierL = fopen( fnameL, "r");
        int tab[257];
        while(1)
        {
            caractereActuel = fgetc(fichierL);
            if( caractereActuel == EOF)
                break;
            code( file.premier, caractereActuel, tab, 0, fichierE);
            ajoutElement( &list2, caractereActuel);
        }
        trierList( list2);
        afficherCodeCaractere( file.premier, caractereActuel, tab, 0, list2);

        return 1;
    }

    return 0;
}
